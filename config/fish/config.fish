# Console Do Not Track: https://consoledonottrack.com
set -x AZURE_CORE_COLLECT_TELEMETRY 0
set -x DO_NOT_TRACK 1
set -x DOTNET_CLI_TELEMETRY_OPTOUT 1
set -x GATSBY_TELEMETRY_DISABLED 1
set -x HOMEBREW_NO_ANALYTICS 1
set -x SAM_CLI_TELEMETRY 0
set -x STNOUPGRADE 1

# Node
set -x NPM_PACKAGES $HOME/.npm_packages
set -x PATH $PATH $NPM_PACKAGES/bin
set -x MANPATH $NPM_PACKAGES/share/man $MANPATH

# Go
set -x GOPATH $HOME/go
set -x GO111MODULE on
set -x GOBIN $GOPATH/bin
set -x GOPRIVATE github.com/safedns
set -x PATH $PATH /usr/local/go/bin $GOBIN
