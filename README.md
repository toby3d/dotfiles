# .files
Recently I decided to make an attempt to collect my own set of dotfiles.
I will be glad to receive any help and advice on data organization!

## `config`
Configuration files of various utilities and programs, placed in `~/.config`.

## `runcom`
Files used at the start of a user session and placed at the root of the home directory.
