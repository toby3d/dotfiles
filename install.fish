#!/usr/bin/fish

for file in (ls -A ~/.dotfiles/runcom)
  ln -sv ~/.dotfiles/runcom/$file ~
end

for file in (ls -A ~/.dotfiles/config)
  ln -sv ~/.dotfiles/config/$file ~/.config
end
